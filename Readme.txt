Our Project was basically for Public Mass.

Idea
=========
# Any shop keeper can create account and add their products for selling.
# Customer can purchase individually or request for their needed goods.
# After receving request / order the shop keeper will deliver the products through any delivery boy with delivery cost (if they need it).
# There will be Administration section to controll the whole system.
# There will be Manager also to monitoring the whole system.


We covered
============
# Multiple authentication
# All role view.
# Admin Module (Done).
# Shop Owner Module Processing.
# We didn't manage time schedule to completing the other side. :(


Future Plan
=============
# As we were supposed to make some real time module (Notification, One-One Chatting) but we didn't make it due to lackings of time. But we can make it in future.
# We can make the whole project if we get more time.




Authentication
===================
1) create database sj_hack
2) php artisan migrate --seed
3) Customer Login
	Email: customer@mail.com
	Password: 12345678
4) Owner Login
	Email: owner@mail.com
	Password: 12345678
5) Manager Login
	Email: manager@mail.com
	Password: 12345678
6) Admin Login
	Email: admin@mail.com
	Password: 12345678





Extra Bonus Task: (2) Mailing Group
===================================
Whenever The Owner add a new Product, every customer will have a Mail. (We've done this features)
If you see the feature please login as Owner & go to Shop > Product > Add New Product.