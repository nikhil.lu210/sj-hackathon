<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name');
            $table->double('price', 8, 3);
            $table->double('dis_price', 8, 3)->nullable();
            $table->text('note')->nullable();
            $table->bigInteger('shop_id')
                  ->unsigned()->nullable();

            $table->foreign('shop_id')
                  ->references('id')
                  ->on('areas')
                  ->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
        
        DB::statement("ALTER TABLE products ADD avatar MEDIUMBLOB after name");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');

        Schema::table("products", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
