<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('customer_id')
                  ->unsigned();
            $table->foreign('customer_id')
                  ->references('id')
                  ->on('users')
                  ->onDelete('cascade');

            $table->bigInteger('offer_id')
                    ->unsigned()->nullable();
            $table->foreign('offer_id')
                    ->references('id')
                    ->on('offers')
                    ->onDelete('cascade');

            $table->double('discount_amount', 8, 3);
            $table->tinyInteger('status')->default(0);
            


            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');


        Schema::table("orders", function ($table) {
            $table->dropSoftDeletes();
        });
    }
}
