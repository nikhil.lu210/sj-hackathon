<div class="modal slide-in-right modal-right fade " id="add_area">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="side-modal-wrapper">
                <div class="vertical-align">
                    <div class="table-cell">
                        <div class="pdd-horizon-15">
                            <h4>New Area</h4>
                            <hr>

                            {{-- Form Starts --}}
                            <form action="{{ route('admin.settings.location.area.store') }}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label>Area Name <span class="required">*</span></label>
                                    <input type="hidden" name="id" id="hidden_id">
                                    <input autocomplete="off" type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}" placeholder="Bagbari" required>

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <button class="btn btn-warning btn-sm btn-block" type="submit">Assign As <span class="text-bold">NEW Area</span></button>
                            </form>
                            {{-- Form Ends --}}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
