<!--All JavaScript Plugin-->

<!-- jQuery -->
<script src="{{ asset('frontend/js/jquery-2.0.0.min.js') }}" type="text/javascript"></script>

<!-- Bootstrap4 files-->
<script src="{{ asset('frontend/js/bootstrap.bundle.min.js') }}" type="text/javascript"></script>

@yield('script_links')

{{-- Custom JS --}}
<script src="{{ asset('frontend/js/script.js') }}"></script>

@yield('custom_script')
