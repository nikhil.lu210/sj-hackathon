<!-- Footer START -->
<footer class="content-footer">
    <div class="footer">
        <div class="copyright go-right">
        <span>Copyright © 2020 <a href="{{ route('homepage') }}" target="_blank" class="text-warning text-bold">দোকানদারি</a>. All rights reserved || Developed By <b><a href="javacript:void(0);" target="_blank" class="text-danger text-uppercase">PiNikk</a></b></span>
            {{-- <span class="go-right">
                <a href="#" class="text-gray mrg-right-15">Term &amp; Conditions</a>
                <a href="#" class="text-gray">Privacy &amp; Policy</a>
            </span> --}}
        </div>
    </div>
</footer>
<!-- Footer END -->
