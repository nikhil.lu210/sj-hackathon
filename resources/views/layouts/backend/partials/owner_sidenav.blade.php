<!-- Side Nav START -->
<div class="side-nav">
    <div class="side-nav-inner">
        <div class="side-nav-logo">
            <a href="{{ route('owner.dashboard.index') }}">
            <div class="logo logo-dark" style="background-image: url('{{ asset('assets/images/logo/logo.png') }}')"></div>
                <div class="logo logo-white" style="background-image: url('{{ asset('assets/images/logo/logo-white.png') }}')"></div>
            </a>
            <div class="mobile-toggle side-nav-toggle">
                <a href="#">
                    <i class="ti-arrow-circle-left"></i>
                </a>
            </div>
        </div>
        <ul class="side-nav-menu scrollable">
            {{-- Dashboard --}}
            <li class="nav-item {{ Request::is('owner') ? 'active' : '' }}">
                <a class="mrg-top-30" href="{{ route('owner.dashboard.index') }}">
                    <span class="icon-holder">
                            <i class="ti-home"></i>
                        </span>
                    <span class="title">Dashboard</span>
                </a>
            </li>

            {{-- Requests --}}
            <li class="nav-item {{ Request::is('owner/request*') ? 'active' : '' }}">
                <a class="mrg-top-0" href="{{ route('owner.request.index') }}">
                    <span class="icon-holder">
                            <i class="ti-signal"></i>
                        </span>
                    <span class="title">Requests</span>
                </a>
            </li>

            {{-- Online Order --}}
            <li class="nav-item dropdown {{ Request::is('owner/order*') ? 'open' : '' }}">
                <a class="dropdown-toggle" href="javascript:void(0);">
                    <span class="icon-holder">
                        <i class="ti-pin-alt"></i>
                    </span>
                    <span class="title">Online Order</span>
                    <span class="arrow">
                        <i class="ti-angle-right"></i>
                    </span>
                </a>
                <ul class="dropdown-menu">
                    <li  class="{{ Request::is('owner/order/pending*') ? 'active' : '' }}">
                        <a href="{{ route('owner.order.pending') }}">Pending</a>
                    </li>
                    <li  class="{{ Request::is('owner/order/ongoing*') ? 'active' : '' }}">
                        <a href="{{ route('owner.order.ongoing') }}">Ongoing</a>
                    </li>
                    <li  class="{{ Request::is('owner/order/completed*') ? 'active' : '' }}">
                        <a href="{{ route('owner.order.completed') }}">Completed</a>
                    </li>
                    <li  class="{{ Request::is('owner/order/canceled*') ? 'active' : '' }}">
                        <a href="{{ route('owner.order.canceled') }}">Canceled</a>
                    </li>
                </ul>
            </li>

            {{-- Shops --}}
            <li class="nav-item dropdown {{ Request::is('owner/shop*') ? 'open' : '' }}">
                <a class="dropdown-toggle" href="javascript:void(0);">
                    <span class="icon-holder">
                        <i class="ti-shopping-cart-full"></i>
                    </span>
                    <span class="title">Shops</span>
                    <span class="arrow">
                        <i class="ti-angle-right"></i>
                    </span>
                </a>
                <ul class="dropdown-menu">
                    <li  class="{{ Request::is('owner/shop') ? 'active' : '' }}">
                        <a href="{{ route('owner.shop.index') }}">Shop Details</a>
                    </li>
                    <li  class="{{ Request::is('owner/shop/product*') ? 'active' : '' }}">
                        <a href="{{ route('owner.shop.product.index') }}">Products</a>
                    </li>
                </ul>
            </li>

             {{-- Offers --}}
             <li class="nav-item dropdown {{ Request::is('owner/offer*') ? 'open' : '' }}">
                <a class="dropdown-toggle" href="javascript:void(0);">
                    <span class="icon-holder">
                        <i class="ti-star"></i>
                    </span>
                    <span class="title">Offers</span>
                    <span class="arrow">
                        <i class="ti-angle-right"></i>
                    </span>
                </a>
                <ul class="dropdown-menu">
                    <li  class="{{ Request::is('owner/offer/active*') ? 'active' : '' }}">
                        <a href="{{ route('owner.offer.active') }}">Active Offers</a>
                    </li>
                    <li  class="{{ Request::is('owner/offer/inactive*') ? 'active' : '' }}">
                        <a href="{{ route('owner.offer.inactive') }}">Inactive Offers</a>
                    </li>
                    <li  class="{{ Request::is('owner/offer/old*') ? 'active' : '' }}">
                        <a href="{{ route('owner.offer.old') }}">Old Offers</a>
                    </li>
                    <li  class="{{ Request::is('owner/offer/create*') ? 'active' : '' }}">
                        <a href="{{ route('owner.offer.create') }}">Create New Offer</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- Side Nav END -->
