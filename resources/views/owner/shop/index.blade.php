@extends('layouts.backend.app')

@section('page_title', 'SHOPS | Shop Details')

@section('css_links')
    {{--  External CSS  --}}
    <link rel="stylesheet" href="{{ asset('assets/vendors/selectize/dist/css/selectize.default.css') }}" />
@endsection

@section('custom_css')
    {{--  External CSS  --}}
    <style>
        /* Custom CSS Here */
        .form-control{
            padding: 0.700rem 0.75rem;
        }
        .selectize-input {
            padding: 0.640rem 0.75rem;
        }

        .responsive-image{
            border-radius: 5px;
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
            height: 80px;
            width: 100%;
        }
        .card-block.task-file.p-0 {
            height: 80px;
        }
        .selectize-dropdown.single{
            z-index: 99999999;
        }
        .selectize-dropdown [data-selectable], .selectize-dropdown .optgroup-header {
            padding: 15px 8px !important;
        }
        a.deactive{
            cursor: pointer;
        }
        li.social-media-list{
            border: 0px !important;
            border-bottom: 1px solid #e6ecf5 !important;
            margin-bottom: 10px;
        }
        li.social-media-list:last-child{
            border: 0px !important;
            border-bottom: 0px solid #e6ecf5 !important;
            margin-bottom: 0px;
        }
    </style>
@endsection

@section('main_content')
<div class="container-fluid">
    <div class="page-title">
        <h4>Shop Details</h4>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <form action="{{ route('owner.shop.update') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="card">
                    <div class="card-block p-25">
                        <div class="card">
                            <div class="card-heading">
                                <h4 class="card-title float-right">Shop Details</h4>

                                <a href="#" class="btn btn-dark btn-sm float-left m-b-0">Edit Info</a>
                            </div>
                            <hr class="m-t-0">
                            <div class="card-block p-25">
                                <div class="row justify-content-center m-b-20">
                                    <div class="col-lg-4 text-center">
                                        <label for="img-upload" class="pointer">
                                            <img id="img-preview" src="{{($shop != null && $shop->logo != null) ? $shop->logo : asset('assets/images/others/img-10.jpg') }}"  width="117" alt="">

                                            <span class="btn btn-default display-block no-mrg-btm">{{ __('Choose Logo') }}</span>
                                            <input class="d-none @error('logo') is-invalid @enderror" type="file" name="logo" accept=".png, .jpg, .jpeg" id="img-upload" value="{{ old('logo') }}">
                                        </label>
                                        @error('logo')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Shop Name <span class="required">*</span></label>
                                            <input autocomplete="off" type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ ($shop != null)? $shop->name:null }}" placeholder="{{ __('Shop Name') }}" required>

                                            @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Country</label>
                                            <input autocomplete="off" type="text" name="country" class="form-control @error('country') is-invalid @enderror" value="Bangladesh" placeholder="{{ __('Shop Name') }}" readonly>

                                            @error('country')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>City <span class="required">*</span></label>
                                            <div class="mrg-top-0">
                                                <select class="selectize-group @error('city_id') is-invalid @enderror" name="city_id" id="shop_city" required>
                                                    <option value="" disabled>Select City</option>
                                                    @if($shop != null && $shop->city_id != null)
                                                        <option value="{{ $shop->city_id }}" selected>{{ $shop->city->name}}</option>
                                                    @endif
                                                    @foreach ($cities as $city)
                                                        <option value="{{ $city->id }}">{{ $city->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            @error('city_id')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Area <span class="required">*</span></label>
                                            <div class="mrg-top-0">
                                                <select class="@error('area') is-invalid @enderror" name="area" id="shop_area">
                                                    @if($shop != null && $shop->area_id != null)
                                                        <option value="{{ $shop->area_id }}" selected>{{ $shop->area->name }}</option>
                                                    @endif
                                                </select>
                                            </div>

                                            @error('area')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer border top">
                        <ul class="list-unstyled list-inline float-right">
                            <li class="list-inline-item">
                                <button type="submit" class="btn btn-dark btn-sm text-bold">
                                    <i class="ti-save"></i>
                                    Update Shop Info
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
            </form>
        </div>
    </div>

    {{-- @include('superadmin.system.modals.team.update_modal') --}}
</div>
@endsection


@section('script_links')
    {{--  External Javascript Links --}}
    <script src="{{ asset('assets/vendors/selectize/dist/js/standalone/selectize.min.js') }}"></script>
    <script src="{{ asset('assets/js/forms/form-elements.js') }}"></script>

@endsection

@section('custom_script')
    {{--  External Custom Javascript  --}}
    <script>
        // $('.selectize-group').selectize({
        //     sortField: 'text'
        // });
        // Custom Script Here
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    // $('#img-preview').css('background-image', 'url('+e.target.result +')');
                    $('#img-preview').attr('src', e.target.result);
                    $('#img-preview').hide();
                    $('#img-preview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#img-upload").change(function() {
            readURL(this);
        });
    </script>

    <script>
        $(document).ready(function(){
            $('select#shop_city').change(function(){
                var city_id = $(this).val();

                $.ajax({
                    method: 'GET',
                    url: '/owner/shop/getarea/'+city_id,
                    datatype: "JSON",
                    success: function(data) {
                        var str = '';
                        for(var i =0; i<data.length; i++){
                            str+= '<option value="'+data[i].id+'">'+data[i].name+'</option>';
                        }
                        console.log(str);
                        $('#shop_area').append(str);

                    },
                    error: function() {
                        console.log("soemhting worong");
                    }
                });

            });
        });
    </script>
@endsection
