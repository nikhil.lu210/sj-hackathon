{{-- Upload File Modal --}}
<div class="modal fade" id="add_product">
    <form action="{{ route('owner.shop.product.store') }}" method="post" id="add_product_from" enctype="multipart/form-data">
        @csrf
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="float-left"><b><span id="social_name">{{ __('Add Inventory Item') }}</span></b></h4>
                    <button class="btn btn-default btn-icon btn-rounded p-l-8 p-r-7 p-t-6 p-b-4" data-dismiss="modal">
                        <i class="ti-close"></i>
                    </button>
                </div>
                <div class="modal-body media-details-modal">
                    <div class="row justify-content-center m-b-20">
                        <div class="col-lg-4 text-center">
                            <label for="img-upload" class="pointer">
                                <img id="img-preview" src="{{ asset('assets/images/others/img-10.jpg') }}"  width="117" alt="">

                                <span class="btn btn-default display-block no-mrg-btm">{{ __('Inventory Item Photo') }}</span>
                                <input class="d-none @error('avatar') is-invalid @enderror" type="file" name="avatar" accept=".png, .jpg, .jpeg" id="img-upload" value="{{ old('avatar') }}">
                            </label>
                            @error('avatar')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label>{{ __('Item Name') }} <span class="required">*</span></label>
                                <input autocomplete="off" type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}" placeholder="{{ __('Item Name') }}" required>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label>{{ __('Select Category') }} <span class="required">*</span></label>
                                <div class="mrg-top-0">
                                    <select class="@error('category_id') is-invalid @enderror" name="category_id" required>
                                        <option value="" disabled selected>{{ __('Select Category') }}</option>
                                        @foreach ($categories as $item)
                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                @error('category_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3">
                            <div class="form-group">
                                <label>{{ __('Item Price') }} <span class="required">*</span></label>
                                <input autocomplete="off" type="number" name="price" class="form-control @error('price') is-invalid @enderror" value="{{ old('price') }}" placeholder="{{ __('Item Price') }}" required>

                                @error('price')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3">
                            <div class="form-group">
                                <label>{{ __('Discount Price') }}</label>
                                <input autocomplete="off" type="number" name="dis_price" class="form-control @error('dis_price') is-invalid @enderror" value="{{ old('dis_price') }}" placeholder="{{ __('Discount Price') }}">

                                @error('dis_price')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label>{{ __('Description') }}</label>
                                <textarea autocomplete="off" type="text" name="description" class="form-control @error('description') is-invalid @enderror" placeholder="{{ __('Description') }}" rows="3">{{ old('description') }}</textarea>

                                @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer border top">
                    <ul class="list-unstyled list-inline pull-right">
                        <li class="list-inline-item">
                            <button type="submit" id="new_item_submit" class="btn btn-dark btn-sm text-bold">
                                <i class="ti-save"></i>
                                {{ __('Add Item') }}
                            </button>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </form>
</div>
