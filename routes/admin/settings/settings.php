<?php
// Settings
Route::group([
    'prefix' => 'settings', //URL
    'as' => 'settings.', //Route
    'namespace' => 'Settings', // Controller
],
    function(){

        // create manager form and store route
        Route::get('/create_manager', 'SettingsController@createManager')->name('manager.create');
        Route::post('/create_manager', 'SettingsController@storeManager')->name('manager.store');

        // create restaurant category form and store route
        Route::get('/create_product_category', 'SettingsController@createCategory')->name('category.create');
        Route::post('/create_product_category', 'SettingsController@storeCategory')->name('category.store');
        Route::post('/update_product_category', 'SettingsController@updateCategory')->name('category.update');

        Route::get('/change_status/{user_id}/{role}', 'SettingsController@changeStatus')->name('change_status');

        
        Route::get('/location', 'SettingsController@locationIndex')->name('location.index');
        Route::post('/location/add_city', 'SettingsController@locationCityStore')->name('location.city.store');
        Route::post('/location/add_area', 'SettingsController@locationAreaStore')->name('location.area.store');
    }
);
