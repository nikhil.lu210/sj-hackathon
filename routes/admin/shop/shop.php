<?php
// Shop
Route::group([
    'prefix' => 'shop', //URL
    'as' => 'shop.', //Route
    'namespace' => 'Shop', // Controller
],
    function(){
        Route::get('/active', 'ShopController@active')->name('active');

        Route::get('/inactive', 'ShopController@inactive')->name('inactive');
    }
);
