<?php
// Order
Route::group([
    'prefix' => 'order', //URL
    'as' => 'order.', //Route
    'namespace' => 'Order', // Controller
],
    function(){
        Route::get('/pending', 'OrderController@pendingOrder')->name('pending');
        Route::get('/ongoing', 'OrderController@ongoingOrder')->name('ongoing');
        Route::get('/completed', 'OrderController@completedOrder')->name('completed');
        Route::get('/canceled', 'OrderController@canceledOrder')->name('canceled');
    }
);
