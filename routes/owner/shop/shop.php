<?php
// shop
Route::group([
    'prefix' => 'shop', //URL
    'as' => 'shop.', //Route
    'namespace' => 'Shop', // Controller
],
    function(){
        Route::get('/', 'ShopController@shopIndex')->name('index');
        Route::post('/update', 'ShopController@updateShop')->name('update');

        Route::get('/getarea/{city_id}', 'ShopController@getArea')->name('getarea');

        Route::get('/products', 'ShopController@productIndex')->name('product.index');
        Route::post('/products', 'ShopController@productStore')->name('product.store');
    }
);
