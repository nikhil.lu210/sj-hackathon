<?php
// Request
Route::group([
    'prefix' => 'request', //URL
    'as' => 'request.', //Route
    'namespace' => 'Request', // Controller
],
    function(){
        Route::get('/', 'RequestController@index')->name('index');
    }
);
