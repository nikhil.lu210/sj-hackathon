<?php
// offer
Route::group([
    'prefix' => 'offer', //URL
    'as' => 'offer.', //Route
    'namespace' => 'Offer', // Controller
],
    function(){
        Route::get('/active', 'OfferController@activeOffer')->name('active');
        Route::get('/inactive', 'OfferController@inactiveOffer')->name('inactive');
        Route::get('/old', 'OfferController@oldOffer')->name('old');
        Route::get('/create', 'OfferController@createOffer')->name('create');
    }
);
