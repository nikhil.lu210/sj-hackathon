<?php

// Owner Routes
Route::group([
    'prefix' => 'owner', // URL
    'as' => 'owner.', // Route
    'namespace' => 'Owner', // Controller
],
    function(){
        /* ==================================
        ============< Dashboard >============
        ===================================*/
        // Dashboard
        include_once 'dashboard/dashboard.php';

        // request
        include_once 'request/request.php';

        // order
        include_once 'order/order.php';

        // shop
        include_once 'shop/shop.php';

        // offer
        include_once 'offer/offer.php';
    }
);
