<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    
     /**
     * Relation with order one to many relationship
     *
     * One user may provide multiple order
     */
    public function owner(){
        return $this->hasOne('App\User', 'owner_id', 'id');
    }

     /**
     * Relation with order one to many relationship
     *
     * One user may provide multiple order
     */
    public function city(){
        return $this->belongsTo('App\Models\City', 'city_id', 'id');
    }

     /**
     * Relation with order one to many relationship
     *
     * One user may provide multiple order
     */
    public function area(){
        return $this->belongsTo('App\Models\Area', 'area_id', 'id');
    }
}
