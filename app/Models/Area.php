<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //
    ];

    
    public function city(){
        return $this->belongsTo('App\Models\City', 'city_id', 'id');
    }


     /**
     * Relation with order one to many relationship
     *
     * One user may provide multiple order
     */
    public function shop(){
        return $this->hasMany('App\Models\Shop', 'area_id', 'id');
    }
}
