<?php

namespace App\Http\Controllers\Owner\Shop;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Shop;
use App\Models\City;
use App\Models\Area;
use App\Models\Category;
use App\Models\Product;
use App\User;
use Mail;

class ShopController extends Controller
{
    /**
     * shopIndex
     * @return shopIndex
     */
    public function shopIndex()
    {
        $shop = Shop::with(['city', 'area'])->where('owner_id', auth()->user()->id)->first();
        $cities = City::all();
        return  view('owner.shop.index', compact(['shop', 'cities']));
    }


    /**
     * shopUpdate
     */
    public function updateShop(Request $request)
    {
        $this->validate($request, array(
            'name' => 'required | string',
            'city_id' => 'required',
        ));
        $shop = Shop::where('owner_id', auth()->user()->id)->first();
        if($shop == null) $shop = new Shop();

        if($request->hasFile('logo')){

            $this->validate($request, array(
                "logo" => 'required | image | mimes:jpeg,png,jpg,gif,svg | max:1024',
            ));

            $image_data=file_get_contents($request->logo);

            $encoded_image=base64_encode($image_data);

            $shop->logo = 'data:image/png;base64,'.$encoded_image;
        }

        $shop->owner_id = auth()->user()->id;
        $shop->name = $request->name;
        $shop->address = 'demo demo';

        if($request->city_id != null)
            $shop->city_id = $request->city_id;

        if($request->area_id != null)
            $shop->area_id = $request->area_id;

        if($shop->save()){
            toast('shop Updated Successfully', 'success')->autoClose(5000)->timerProgressBar();
        } else{
            toast('shop Does not Updated', 'error')->autoClose(5000)->timerProgressBar();
        }
        return redirect()->back();
    }



    /**
     * productIndex
     * @return productIndex
     */
    public function productIndex()
    {
        $categories = Category::all();
        $products = Product::where(auth()->user()->shop->id)->get();
        return  view('owner.shop.product', compact('categories', $products));
    }


    /**
     * productIndex
     * @return getArea
     */
    public function getArea($city_id)
    {
        $areas = Area::where('city_id', $city_id)->get();
        return  response()->json($areas);
    }



    public function productStore(Request $request)
    {
        $this->validate($request, array(
            'name'  =>  'required',
            'price'  =>  'required',
        ));

        $product = new Product();
        $product->name = $request->name;
        $product->shop_id =auth()->user()->shop->id;
        $product->price = $request->price;
        if($request->dis_price) $product->dis_price = $request->dis_price;
        if($request->description) $product->note = $request->description;

        if($request->hasFile('avatar')){

            $this->validate($request, array(
                "avatar" => 'required | image | mimes:jpeg,png,jpg,gif,svg | max:1024',
            ));

            $image_data=file_get_contents($request->avatar);

            $encoded_image=base64_encode($image_data);

            $product->avatar = 'data:image/png;base64,'.$encoded_image;
        }

        if($product->save()){
            $customers = User::where('role_id', 4)->get();
            foreach($customers as $customer){

                $data = [
                    'name'	 =>	$request->name,
                    'email'	 => $customer->email,
                    'subject' => "new Product add Information"
                ];
    

                Mail::send('emails.mail', $data, function($message) use ($data) {
                    $message->to($data['email']);
                    $message->subject($data['subject']);
                    // $message->from('sperrowmailtest@gmail.com');
                    $message->from('sperrowmailtest@gmail.com', 'dukandari');
                });
    
            }
            toast('product created Successfully', 'success')->autoClose(5000)->timerProgressBar();
        } else{
            toast('product Does not Updated', 'error')->autoClose(5000)->timerProgressBar();
        }
        return redirect()->back();


    }



}
