<?php

namespace App\Http\Controllers\Owner\Order;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    /**
     * Constructor
     *
     * @return Middleware
     */
    public function __construct()
    {
        $this->middleware(['auth', 'owner']);
    }

    /**
     * pendingOrder
     *
     * @return pendingOrder
     */
    public function pendingOrder()
    {
        return view('owner.order.pending');
    }

    /**
     * ongoingOrder
     *
     * @return ongoingOrder
     */
    public function ongoingOrder()
    {
        return view('owner.order.ongoing');
    }

    /**
     * completedOrder
     *
     * @return completedOrder
     */
    public function completedOrder()
    {
        return view('owner.order.completed');
    }

    /**
     * canceledOrder
     *
     * @return canceledOrder
     */
    public function canceledOrder()
    {
        return view('owner.order.canceled');
    }
}
