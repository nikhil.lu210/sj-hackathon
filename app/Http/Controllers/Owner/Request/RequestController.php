<?php

namespace App\Http\Controllers\Owner\Request;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RequestController extends Controller
{
    /**
     * Constructor
     *
     * @return Middleware
     */
    public function __construct()
    {
        $this->middleware(['auth', 'owner']);
    }

    /**
     * Owner request Index
     *
     * @return Ownerrequest
     */
    public function index()
    {
        return view('owner.request.index');
    }
}
