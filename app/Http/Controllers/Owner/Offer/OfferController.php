<?php

namespace App\Http\Controllers\Owner\Offer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Category;
use App\Models\Restaurant;

class OfferController extends Controller
{
    /**
     * activeOffer
     * @return activeOffer
     */
    public function activeOffer()
    {
        return  view('owner.offer.active');
    }
    
    /**
     * inactiveOffer
     * @return inactiveOffer
     */
    public function inactiveOffer()
    {
        return  view('owner.offer.inactive');
    }
    
    /**
     * oldOffer
     * @return oldOffer
     */
    public function oldOffer()
    {
        return  view('owner.offer.old');
    }
    
    /**
     * createOffer
     * @return createOffer
     */
    public function createOffer()
    {
        return  view('owner.offer.create');
    }
}
