<?php

namespace App\Http\Controllers\Admin\Shop;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Category;
use App\Models\Restaurant;

class ShopController extends Controller
{
    /**
     * for showing active manager
     * @return shop lists active
     */
    public function active()
    {
        return  view('admin.shop.active');
    }

    /**
     * for showing active manager
     * @return shop lists
     */
    public function inactive()
    {
        return  view('admin.shop.inactive');
    }

    /**
     * for showing active manager
     * @return RestaurantCategory lists
     */
    public function categories()
    {
        $categories = Category::with(['shop'])->get();
        return  view('admin.shop.categories', compact(['categories']));
    }
}
